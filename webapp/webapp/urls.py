from django.urls import path

from contacts.views import add_contact,home, index, delete_contact

urlpatterns = [
    path('', index, name="index"),
    path('add/', add_contact, name="add-contact"),
    path('delete/', delete_contact, name="delete-contact"),
]
